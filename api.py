import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib

from tensorflow.keras.models import load_model
from sklearn.preprocessing import StandardScaler
from category_encoders import BinaryEncoder

from flask import Flask, request, jsonify
from flask_cors import CORS

api = Flask(__name__)
CORS(api)

@api.route('/api/predictfuture', methods=['POST'])
def predict_future():
    data = request.get_json()
    # print(data)
    StudentId = data.get('StudentId')
    FirstName = data.get('FirstName')
    LastName = data.get('LastName')
    AcademicDataFile = data.get('academicDataFile')

    academicData = pd.read_csv('.'+AcademicDataFile)
    reference = pd.read_csv("./Reference/precleaned_file.csv")
    
    # encoding process
    categorical_features = ['Grade_LEVEL','written_remarks', 'performance_remarks']
    academicData['sex_encoded'] = np.where(academicData['Sex']=="F", 0, 1)
    academicData = academicData.drop(columns=['Sex'])
    encoder = BinaryEncoder(cols=categorical_features)
    X_encoded = encoder.fit_transform(reference[categorical_features])
    _input = academicData.drop(categorical_features, axis=1)
    _input = _input.reset_index(drop=True)
    X_encoded = X_encoded.reset_index(drop=True)
    _input = pd.concat([_input, X_encoded], axis=1)
    _input = _input.drop(columns=['first_name','last_name'])
    _input = _input.reset_index(drop=True)
    _input = _input.dropna()

    # scaling process
    scaler = StandardScaler()
    _input = scaler.fit_transform(_input)
    print(_input)

    # load the model
    model = load_model("NN_o_subject_feature_final.pb")
    predictions = model.predict(_input)
    prediction_items = [str(round(x[0]*100,3)) for x in predictions]
    print([x for x in prediction_items])
    
    return jsonify({
        "StudentId":StudentId,
        "FirstName":FirstName,
        "LastName":LastName,
        "AcademicDataFile":AcademicDataFile,
        "predictionItems":prediction_items})

if __name__ == '__main__':
    api.run(port=5000)
